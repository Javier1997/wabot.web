import Vue from 'vue'
import VueRouter from 'vue-router'
import Landingpage from '../views/landingpage.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Landingpage',
    component: Landingpage
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
